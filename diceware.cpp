#include <iostream>
#include <fstream>
#include <random>
#include <string>
#include <cstring>
#include <array>
#include "urandom_generator.hpp"
using std::string;
using std::cout;
using std::endl;
using std::ifstream;
using std::array;

constexpr int DEFAULT_WORDS     = 6;
constexpr int DEFAULT_PASSWORDS = 1;
constexpr size_t DICT_SIZE      = 7776;

enum ERRORS {
	SUCCESS = 0,
	CANT_OPEN_DICTIONARY,
	WRONG_DICT_SIZE
};

int main(int argc, char **argv)
{
	int words     = DEFAULT_WORDS;
	int passwords = DEFAULT_PASSWORDS;
	const string dict_filename  = "words.txt";

	// check arguments
	if (argc > 1) {
		if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
			cout <<  "Usage:\n\tdiceware [words=6] [passwords=1]" << '\n';
			return 0;
		}
		try {
			const int tmp_words = std::stoi(argv[1]);
			if (tmp_words > 0) words = tmp_words;
		}
		catch (std::invalid_argument&) {
			cout << "cannot convert '" << argv[1]
				<< "' to number, using default value for words: "
				<< DEFAULT_WORDS << '\n';
		}
	}
	if (argc > 2) {
		try {
			const int tmp_passwords = std::stoi(argv[2]);
			if (tmp_passwords > 0) passwords = tmp_passwords;
		}
		catch (std::invalid_argument&) {
			cout << "cannot convert '" << argv[2]
				<< "' to number, using default value for passwords: "
				<< DEFAULT_PASSWORDS << '\n';
		}
	}

	ifstream dict_file(dict_filename);
	if (not dict_file.is_open()) {
		cout << "dictionary file " << dict_filename << " cannot be opened" << endl;
		return CANT_OPEN_DICTIONARY;
	}

	// read dictionary
	array<string, DICT_SIZE> dict;
	size_t dict_i = 0;
	for (string tmp_str; getline(dict_file, tmp_str) && dict_i < DICT_SIZE;) {
		dict.at(dict_i++) = std::move(tmp_str);
	}
	if (dict_i != DICT_SIZE) {
		cout << "dictionary size is not equal to 7776, something wrong with dictionary file, exiting...\n";
		return WRONG_DICT_SIZE;
	}

	// generate password
	Urandom gen;
	std::uniform_int_distribution<int> dice(0, DICT_SIZE - 1);
	for (int i = 0; i < passwords; ++i) {
		bool first = true;
		for (int j = 0; j < words; ++j) {
			if (first) {
				first = false;
				cout << dict.at(dice(gen));
			}
			else {
				cout << '-' << dict.at(dice(gen));
			}
		}
		cout << '\n';
	}
	return SUCCESS;
}

