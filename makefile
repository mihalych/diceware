opts = -std=c++20 -O2 -Wall -Werror
cxx  = clang++

all: diceware mpgen

diceware: diceware.cpp urandom_generator.hpp
	${cxx} ${opts} diceware.cpp -o diceware

mpgen: mpgen.cpp urandom_generator.hpp
	${cxx} ${opts} mpgen.cpp -o mpgen

clean:
	rm -rfv *.o diceware mpgen

