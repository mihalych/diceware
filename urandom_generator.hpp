#ifndef GENERATOR_HPP
#define GENERATOR_HPP

#include <fstream>
#include <limits>
#include <stdexcept>
#include <cstdint>

// std::mt19937 replacement
// uses /dev/urandom as entropy source
class Urandom {
	public:
		using result_type = uint32_t;

		Urandom()
		{
			if (not m_devurandom.is_open()) {
				throw(std::runtime_error("cant open /dev/urandom"));
			}
			fill();
		}

		result_type operator()() {
			if (m_counter == BUFFER_SIZE) [[unlikely]] fill();
			return m_array[m_counter++];
		}

		static constexpr result_type min() {
			return std::numeric_limits<result_type>::min();
		}
		static constexpr result_type max() {
			return std::numeric_limits<result_type>::max();
		}

	private:
		std::ifstream m_devurandom       { "/dev/urandom", std::ios::binary };
		int m_counter                    { 0 };
		constexpr static int BUFFER_SIZE { 1024 };
		result_type  m_array[BUFFER_SIZE];

		void fill() {
			m_devurandom.read(
					reinterpret_cast<char*>(m_array),
					sizeof(result_type) * BUFFER_SIZE );
			m_counter = 0;
		}
};

#endif // GENERATOR_HPP

