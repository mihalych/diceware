#include <iostream>
#include <random>
#include <cstring>
#include <array>
#include "urandom_generator.hpp"
using std::cout;
using std::string;
using std::array;

const int DEFAULT_LEN       = 18;
const int DEFAULT_PASSWORDS = 1;

int main(int argc, char **argv)
{
	int len       = DEFAULT_LEN;
	int passwords = DEFAULT_PASSWORDS;

	// check arguments
	if (argc > 1) {
		if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
			cout <<  "Usage:\n\tmpgen [len=18] [passwords=1]" << '\n';
			return 0;
		}
		try {
			const int tmp = std::stoi(argv[1]);
			if (tmp > 0) len = tmp;
		}
		catch (std::invalid_argument&) {
			// cout << "cannot convert '" << argv[1] << "' to number, using default value for len: " << DEFAULT_LEN << '\n';
		}
	}
	if (argc > 2) {
		try {
			const int tmp = std::stoi(argv[2]);
			if (tmp > 0) passwords = tmp;
		}
		catch (std::invalid_argument&) {
			// cout << "cannot convert '" << argv[2] << "' to number, using default value for passwords: " << DEFAULT_PASSWORDS << '\n';
		}
	}

	// generate password
	constexpr unsigned ALPHABET_SIZE { 62 };
	const array<char, ALPHABET_SIZE> alphabet = [] () {
		array<char, ALPHABET_SIZE> result{};
		int counter = 0;
		char letter;
		for (letter = 'A'; letter <= 'Z'; letter++) result[counter++] = letter;
		for (letter = 'a'; letter <= 'z'; letter++) result[counter++] = letter;
		for (letter = '0'; letter <= '9'; letter++) result[counter++] = letter;
		return result;
	}();
	Urandom gen;
	std::uniform_int_distribution<int> char_dist(0, ALPHABET_SIZE - 1);
	for (int i = 0; i < passwords; ++i) {
		for (int j = 0; j < len; ++j) {
			cout << alphabet[char_dist(gen)];
		}
		cout << '\n';
	}
	return 0;
}

